import React from 'react'

import items from 'data/items'
import { TagsInput } from 'components/tags-input'

import './App.css'

const App = () => (
	<>
		<h1>Tags input demo</h1>

		<TagsInput
			suggestions={items}
		/>

		<p>Content after tags input.</p>
	</>
)

export default App
