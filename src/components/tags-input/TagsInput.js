import React from 'react'

import './TagsInput.css'

const TagsInput = () => {
	return (
		<div className="tags-input">
			<ul className="input">
				<li>tag 1</li>
				<li>tag 2</li>
				<li className="new-tag-input">
					<input type="text" />
				</li>
			</ul>
		</div>
	)
}

export default TagsInput
